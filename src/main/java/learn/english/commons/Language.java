package learn.english.commons;

public enum Language {
    TR("Türkçe", "tr"),
    EN("İngilizce", "en");

    private String name;
    private String code;

    private Language(String name, String code) {
        this.name = name;
        this.code = code;
    }

    public static Language getByName(String name) {
        for (Language language : values()) {
            if (language.getName().equalsIgnoreCase(name)) return language;
        }

        return null;
    }

    public static Language getByCode(String code) {
        for (Language language : values()) {
            if (language.getCode().equalsIgnoreCase(code)) return language;
        }

        return null;
    }

    public String getName() {
        return name;
    }

    public String getCode() {
        return code;
    }
}
