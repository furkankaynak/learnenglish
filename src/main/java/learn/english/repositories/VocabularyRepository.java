package learn.english.repositories;

import learn.english.commons.Language;
import learn.english.models.Vocabulary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


public interface VocabularyRepository extends JpaRepository<Vocabulary, Long>, VocabularyRepositoryCustom {
    public Vocabulary findOneByValue(String value);
    public Vocabulary findOneByValueAndLanguage(String value, Language language);
}
