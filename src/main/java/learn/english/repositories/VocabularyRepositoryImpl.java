package learn.english.repositories;

import learn.english.models.Vocabulary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaContext;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;

public class VocabularyRepositoryImpl implements VocabularyRepositoryCustom {

    private final EntityManager em;

    @Autowired
    public VocabularyRepositoryImpl(JpaContext context) {
        this.em = context.getEntityManagerByManagedType(Vocabulary.class);
    }

}
