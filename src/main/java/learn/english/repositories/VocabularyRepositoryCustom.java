package learn.english.repositories;

import learn.english.models.Vocabulary;

import java.util.List;

public interface VocabularyRepositoryCustom extends CustomRepository<Vocabulary> {
}
