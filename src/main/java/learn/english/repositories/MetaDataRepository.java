package learn.english.repositories;

import learn.english.models.MetaData;
import org.springframework.data.repository.CrudRepository;

public interface MetaDataRepository extends CrudRepository<MetaData, Long> {



}
