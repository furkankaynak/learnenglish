package learn.english.controller;

import learn.english.commons.Language;
import learn.english.commons.VocabularyType;
import learn.english.jobs.MostCommonEnglishWordsCrawlerJob;
import learn.english.models.Vocabulary;
import learn.english.models.dtos.TranslateDTO;
import learn.english.services.TurengTranslatorService;
import learn.english.services.VocabularyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.ConstraintViolationException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RestController
public class VocabularyController {

    @Autowired
    private VocabularyService vocabularyService;

    @Autowired
    private TurengTranslatorService turengTranslatorService;

    @Autowired
    private MostCommonEnglishWordsCrawlerJob job;

    @RequestMapping(value = "/search/{word}", method = RequestMethod.GET)
    public TranslateDTO translate(@PathVariable("word") String word) {
        try {
            TranslateDTO translate = turengTranslatorService.traslate(word);
            vocabularyService.saveByTranslateDto(translate, VocabularyType.WORD);
            return translate;
        } catch (Exception e) {
            System.err.println(e);
        }

        return null;
    }

    @RequestMapping(value = "/findAll", method = RequestMethod.GET)
    public List<Vocabulary> findAll() {
        return vocabularyService.findAll();
    }

    @RequestMapping(value = "/job", method = RequestMethod.GET)
    public void job() throws IOException {
        job.run();
    }

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public void createVocab() {
        Vocabulary hello = new Vocabulary("hello", VocabularyType.WORD, Language.EN);
        Vocabulary naber = new Vocabulary("naber", VocabularyType.WORD, Language.TR);
        Vocabulary selam = new Vocabulary("selam", VocabularyType.WORD, Language.TR);
        Vocabulary selamalykm = new Vocabulary("selamun aleykum", VocabularyType.WORD, Language.TR);

        hello.addEqualivientVocab(naber);
        hello.addEqualivientVocab(selam);
        hello.addEqualivientVocab(selamalykm);

        try {
            vocabularyService.save(hello);
        } catch (DataIntegrityViolationException e) {
            System.out.println(e);
        }

        Vocabulary hi = new Vocabulary("hi", VocabularyType.WORD, Language.EN);
        Vocabulary hay = new Vocabulary("hay", VocabularyType.WORD, Language.TR);
        Vocabulary naber2 = new Vocabulary("naber", VocabularyType.WORD, Language.TR);

        hi.addEqualivientVocab(naber2);
        hi.addEqualivientVocab(hay);

        try {
            vocabularyService.save(hi);
        } catch (DataIntegrityViolationException e) {
            System.out.println(e);
        }

        List<Vocabulary> all = vocabularyService.findAll();

        System.out.println(all);
    }

}
