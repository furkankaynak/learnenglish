package learn.english.services.retrofit;

import retrofit2.http.GET;
import retrofit2.http.Path;

public interface TurengSearchService {

    @GET("turkce-ingilizce/{word}")
    public String turkishEnglishSearch(@Path("word") String word);
}
