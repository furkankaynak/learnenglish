package learn.english.services;

import learn.english.commons.Language;
import learn.english.models.dtos.TranslateDTO;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class TurengTranslatorService {

    @Value("${tureng.baseUrl}")
    private String baseUrl;

    private static final String resultTableSelector = "#englishResultsTable";
    private static final String wordSelector = resultTableSelector + " tbody tr";
    private static final String notWordSelector = ".visible-xs.mobile-category-row";
    private static final String suggestionListSelector = ".suggestion-list li";
    private static final int maxResult = 10;

    public TranslateDTO traslate(String word) throws IOException {
        Document document = getDocument(word);
        Elements resultTable = document.select(resultTableSelector);

        return populateResult(document, resultTable).searchingWord(word);
    }

    private TranslateDTO populateResult(Document document, Elements resultTable) {
        if (!resultTable.isEmpty()) {
            return populateIfResultIsNotEmpty(document);
        }
        return populateIfResultIsEmpty(document);
    }

    private TranslateDTO populateIfResultIsEmpty(Document document) {
        TranslateDTO dto = new TranslateDTO();
        Elements suggestions = document.select(suggestionListSelector);
        for (int i = 0; i < suggestions.size() - 1; i++) {
            String suggestion = suggestions.get(i).select("a").text();
            dto.addSuggestion(suggestion);
        }
        return dto;
    }

    private TranslateDTO populateIfResultIsNotEmpty(Document document) {
        Elements words = document.select(wordSelector).not(notWordSelector);
        Language fromLanguage = Language.getByName(words.first().select("th.c2").text());
        Language toLanguage = Language.getByName(words.first().select("th.c3").text());

        TranslateDTO dto = new TranslateDTO().from(fromLanguage).to(toLanguage);
        addMeanings(dto, words);

        return dto;
    }

    private void addMeanings(TranslateDTO dto, Elements words) {
        for (int i = 1; i < words.size(); i++) {
            if (dto.getMeanings().size() >= maxResult) break;
            Elements langBlock = words.get(i).select("td[lang=" + dto.getTo().getCode() + "]");
            if (langBlock.isEmpty()) continue;
            String meaning = langBlock.select("a").text();
            dto.addMeaning(meaning);
        }
    }

    private Document getDocument(String word) throws IOException {
        return Jsoup.connect(baseUrl + word).get();
    }

}
