package learn.english.services;

import learn.english.commons.VocabularyType;
import learn.english.models.Vocabulary;
import learn.english.models.dtos.TranslateDTO;
import learn.english.repositories.VocabularyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service
public class VocabularyService {

    @Autowired
    private VocabularyRepository vocabularyRepository;

    @Transactional
    public Vocabulary save(Vocabulary vocabulary) {
        return vocabularyRepository.save(vocabulary);
    }

    public List<Vocabulary> findAll() {
        return vocabularyRepository.findAll();
    }

    public CompletableFuture saveByTranslateDto(TranslateDTO dto, VocabularyType vocabularyType) {
        if (dto.getMeanings().isEmpty()) return null;
        CompletableFuture futureSave = CompletableFuture.supplyAsync(() -> {
            Vocabulary searchingWord = new Vocabulary(dto.getSearchingWord(), vocabularyType, dto.getFrom());
            for (String meaningWord : dto.getMeanings()) {
                searchingWord.addEqualivientVocab(new Vocabulary(meaningWord, vocabularyType, dto.getTo()));
            }
            return save(searchingWord);
        });
        return futureSave;
    }
}
