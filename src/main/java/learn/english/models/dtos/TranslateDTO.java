package learn.english.models.dtos;

import learn.english.commons.Language;

import java.util.LinkedHashSet;
import java.util.Set;

public class TranslateDTO {

    private Language from;
    private Language to;
    private String searchingWord;
    private Set<String> meanings = new LinkedHashSet<>();
    private Set<String> suggestionList = new LinkedHashSet<>();

    /*
    * Constructors
     */

    public TranslateDTO() {
    }

    public TranslateDTO(Language from, Language to, String searchingWord) {
        this.from = from;
        this.to = to;
        this.searchingWord = searchingWord;
    }

    /*
    * Builders
     */

    public TranslateDTO from(String fromCountry) {
        this.from = Language.getByName(fromCountry);
        return this;
    }

    public TranslateDTO to(String toCountry) {
        this.to = Language.getByName(toCountry);
        return this;
    }

    public TranslateDTO from(Language fromLanguage) {
        this.from = fromLanguage;
        return this;
    }

    public TranslateDTO to(Language toLanguage) {
        this.to = toLanguage;
        return this;
    }

    public TranslateDTO searchingWord(String searchingWord) {
        this.searchingWord = searchingWord;
        return this;
    }

    public void addMeaning(String meaning) {
        getMeanings().add(meaning);
    }

    public void addSuggestion(String suggestion) {
        getSuggestionList().add(suggestion);
    }

    /*
    * Getters And Setters
     */

    public Language getFrom() {
        return from;
    }

    public void setFrom(Language from) {
        this.from = from;
    }

    public Language getTo() {
        return to;
    }

    public void setTo(Language to) {
        this.to = to;
    }

    public String getSearchingWord() {
        return searchingWord;
    }

    public void setSearchingWord(String searchingWord) {
        this.searchingWord = searchingWord;
    }

    public Set<String> getMeanings() {
        return meanings;
    }

    public void setMeanings(Set<String> meanings) {
        this.meanings = meanings;
    }

    public Set<String> getSuggestionList() {
        return suggestionList;
    }

    public void setSuggestionList(Set<String> suggestionList) {
        this.suggestionList = suggestionList;
    }
}
