package learn.english.models;

import learn.english.commons.Language;
import learn.english.commons.VocabularyType;

import javax.persistence.*;
import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Set;

@Entity
@Table(name = "vocabulary")
//uniqueConstraints = { @UniqueConstraint( columnNames = { "value", "language" } ) }
public class Vocabulary implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private boolean ignored = false;

    @Column(name = "value", nullable = false)
    private String value;

    @Enumerated(EnumType.STRING)
    @Column(name = "vocabularyType", nullable = false)
    private VocabularyType vocabularyType;

    @Enumerated(EnumType.STRING)
    @Column(name = "language", nullable = false)
    private Language language;

    @ManyToMany(cascade = CascadeType.ALL)
    private Set<Vocabulary> equalivientVocabs = new LinkedHashSet<>();

    @OneToMany(cascade = CascadeType.ALL)
    private Set<MetaData> metaDataList = new LinkedHashSet<>();

    /*
    * Helper Methods
     */

    public void addEqualivientVocab(Vocabulary vocabulary) {
        vocabulary.getEqualivientVocabs().add(this);
        equalivientVocabs.add(vocabulary);
    }

    public void addMetaData(MetaData metaData) {
        metaDataList.add(metaData);
    }

    /*
    * Constructors
     */

    public Vocabulary() {
    }

    public Vocabulary(String value, VocabularyType vocabularyType, Language language) {
        this.value = value.toLowerCase();
        this.vocabularyType = vocabularyType;
        this.language = language;
    }

    /*
    * Getters and Setters
     */

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isIgnored() {
        return ignored;
    }

    public void setIgnored(boolean ignored) {
        this.ignored = ignored;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value.toLowerCase();
    }

    public VocabularyType getVocabularyType() {
        return vocabularyType;
    }

    public void setVocabularyType(VocabularyType vocabularyType) {
        this.vocabularyType = vocabularyType;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public Set<Vocabulary> getEqualivientVocabs() {
        return equalivientVocabs;
    }

    public void setEqualivientVocabs(Set<Vocabulary> equalivientVocabs) {
        this.equalivientVocabs = equalivientVocabs;
    }

    public Set<MetaData> getMetaDataList() {
        return metaDataList;
    }

    public void setMetaDataList(Set<MetaData> metaDataList) {
        this.metaDataList = metaDataList;
    }

    /*
    * Ovverides
     */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Vocabulary that = (Vocabulary) o;

        if (id != that.id) return false;
        if (ignored != that.ignored) return false;
        if (language != that.language) return false;
        if (value != null ? !value.equals(that.value) : that.value != null) return false;
        if (vocabularyType != that.vocabularyType) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (ignored ? 1 : 0);
        result = 31 * result + (value != null ? value.hashCode() : 0);
        result = 31 * result + (vocabularyType != null ? vocabularyType.hashCode() : 0);
        result = 31 * result + (language != null ? language.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Vocabulary{" +
                "id=" + id +
                ", ignored=" + ignored +
                ", value='" + value + '\'' +
                ", vocabularyType=" + vocabularyType +
                ", country=" + language +
                '}';
    }
}
