package learn.english.models;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class MetaData implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "value", nullable = false)
    private String value;

    @ManyToOne
    private Vocabulary vocabulary;

    /*
    * Constructors
     */

    public MetaData() {
    }

    /*
    * Getters And Setters
     */

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Vocabulary getVocabulary() {
        return vocabulary;
    }

    public void setVocabulary(Vocabulary vocabulary) {
        this.vocabulary = vocabulary;
    }

    /*
    * Overrides
     */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MetaData metaData = (MetaData) o;

        if (id != metaData.id) return false;
        if (name != null ? !name.equals(metaData.name) : metaData.name != null) return false;
        if (value != null ? !value.equals(metaData.value) : metaData.value != null) return false;
        if (vocabulary != null ? !vocabulary.equals(metaData.vocabulary) : metaData.vocabulary != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (value != null ? value.hashCode() : 0);
        result = 31 * result + (vocabulary != null ? vocabulary.hashCode() : 0);
        return result;
    }
}
